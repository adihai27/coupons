import React, { useState } from 'react'
import axios from 'axios'
import Alert from 'react-bootstrap/Alert';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

function LoginTest() {

    const [user, setUser] = useState({ email: '', password: '', gender: '' })

    const [isValid, setIsValid] = useState('true');

    const login2 = (e) => {
        e.preventDefault();
        if (user.email != '' || user.password != '' || user.gender != '') {
            console.log('email: ' + user.email, 'password: ' + user.password, 'Gender: ' + user.gender)
        } else {
            alert("yyy")
        }
    }

    const login = (e) => {
        e.preventDefault()
        axios.post("http://localhost:3001/login", {
            userName: user.email,
            password: user.password,
            radioLogin: user.gender,
        }).then((Response) => {
            if (!Response.data.message) {
                console.log('yes')
                setIsValid(true)

            } else {
                console.log(Response.data.message)
                setIsValid(false)
            }
        })
    }

    return (
        <form >

            <Typography component="h1" variant="h5">
                Sign in
            </Typography>
            <div className="form-group">
                <label>Email</label>
                <input type="email" className="form-control" placeholder="Enter email"
                    onChange={(e) => setUser({ ...user, email: e.target.value })} />
            </div>
            <TextField
                
                id="filled-error"
                label="Password"
                variant="filled"
            />

            <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" placeholder="Enter password" onChange={(e) => setUser({ ...user, password: e.target.value })} />
            </div>

            <FormControl component="fieldset">
                <FormLabel component="legend">Department</FormLabel>
                <RadioGroup row aria-label="position" name="position" onChange={(e) => setUser({ ...user, gender: e.target.value })}>

                    <FormControlLabel
                        value="Admin"
                        control={<Radio color="primary" />}
                        label="Admin"
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="Company"
                        control={<Radio color="primary" />}
                        label="Company"
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="Customer"
                        control={<Radio color="primary" />}
                        label="Customer"
                        labelPlacement="start"
                    />

                </RadioGroup>
                <Button type='submit' variant="contained" onClick={login2}>Login</Button>
                {/* <button type='submit'
                className="btn btn-dark btn-lg btn-block custome-btm">Sign in</button> */}
                <div className="form-group">
                    <p className="forgot-password text-right">
                        Need Acount <a href="/sign-up">Signup</a>
                    </p>

                </div>
            </FormControl>

            {/* <div >
                <input type="radio" value="Male" name="gender" onClick={() => setGender('Male')} /> Male
                <input type="radio" value="Female" name="gender" onClick={() => setGender('Female')} /> Female
                <input type="radio" value="Other" name="gender" onClick={() => setGender('Other')} /> Other
            </div> */}


        </form>
    );
}
export default LoginTest
