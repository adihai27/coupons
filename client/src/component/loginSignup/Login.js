import React, { Component } from 'react'
import LogoutButton from './LogoutButton'
//import { useAuth0 } from '@auth0/auth0-react'
import Profile from '../Profile'

function Login() {

   // const { loginWithRedirect, isAuthenticated } = useAuth0()

    return (
        <div>
            <form>
                <h3>Sign In</h3>
                <div className="form-group">
                    <label>Email Address</label>
                    <input type="email" className="form-control" placeholder="Enter Email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" />
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>
                
                <button id="loginBtn"  type="submit" className="btn btn-primary btn-block" onClick={}>Submit</button>
               
                <p className="forgot-password text-right">
                    Forgot <a href="#">password?</a>
                </p>

            </form>
            <Profile />

        </div>
    )
}

export default Login

