import React, { useState } from 'react'
import axios from 'axios'
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';


function RegisterTest() {

    const [details, setDetails] = useState({ firstName: '', lastName: '', email: '', password: '', gender: '' })

    const register = (e) => {
        e.preventDefault()
        axios.post("http://localhost:3001/register", {
            firstName: details.firstName,
            lastName: details.lastName,
            email: details.email,
            password: details.password,
            gender: details.gender,
        }).then((Response) => {
            console.log(Response)
            console.log("sucsses to register")
        })
    }

    return (
        <form>
            <Typography component="h1" variant="h5">
                Sign up
            </Typography>

            <div className="form-group">
                <label>First name</label>
                <input type="text"
                    className="form-control"
                    placeholder="First name"
                    onChange={(e) => setDetails({ ...details, firstName: e.target.value })} />
            </div>

            <div className="form-group">
                <label>Last name</label>
                <input type="text"
                    className="form-control"
                    placeholder="Last name"
                    onChange={(e) => setDetails({ ...details, lastName: e.target.value })} />
            </div>

            <div className="form-group">
                <label>Email</label>
                <input type="email"
                    className="form-control"
                    placeholder="Enter email"
                    onChange={(e) => setDetails({ ...details, email: e.target.value })} />
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password"
                    className="form-control"
                    placeholder="Enter password"
                    onChange={(e) => setDetails({ ...details, password: e.target.value })} />
            </div>

            <FormControl component="fieldset">
                <FormLabel component="legend">Department</FormLabel>
                <RadioGroup row aria-label="position"
                    name="position" defaultValue="top"
                    onChange={(e) => setDetails({ ...details, gender: e.target.value })}>

                    <FormControlLabel
                        value="Admin"
                        control={<Radio color="primary" />}
                        label="Admin"
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="Company"
                        control={<Radio color="primary" />}
                        label="Company"
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="Customer"
                        control={<Radio color="primary" />}
                        label="Customer"
                        labelPlacement="start"
                    />

                </RadioGroup>
                <Button type='submit' variant="contained" >Sign up</Button>
                {/* <button type='submit'
            className="btn btn-dark btn-lg btn-block custome-btm">Sign in</button> */}

            </FormControl>

            <p className="forgot-password text-right">
                Already registered <a href="/">log in?</a>
            </p>
        </form>
    );
}
export default RegisterTest