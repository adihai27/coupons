import React from 'react'
import { useAuth0 } from '@auth0/auth0-react'



const LoginButton = () => {
    const { logout } = useAuth0()



    return (
        <button type="submit" className="btn btn-primary btn-block" onClick={() => logout()}>
            Logout
        </button>
    )
}

export default LoginButton
