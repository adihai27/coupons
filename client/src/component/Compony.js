import React, { useState, useEffect } from 'react'
import axios from 'axios'

function Compony() {

    // const [movieName, setMovieName] = useState('')
    // const [review, setReview] = useState('')
    const [compName, setCompName] = useState('')
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    // const [movieReviewList, setMovieReviewList] = useState([])

    // useEffect(() => {
    //   axios.get("http://localhost:3001/api/get").then((response) => {
    //     console.log(response.data)
    //     setMovieReviewList(response.data)
    //   })

    // }, [])

    const submitReview = () => {
        axios.post("http://localhost:3001/api/insert", {
            compName: compName,
            password: password,
            email: email,
        })

        // setMovieReviewList([
        //   ...movieReviewList,
        //   { movieName: movieName, movieReview: review }
        // ])
    }

    return (
        <div>
            <form >
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Company Name</label>
                    <input type="text"
                        className="form-control"
                        placeholder="Enter Company Name"
                        onChange={(e) => {
                            setCompName(e.target.value)
                        }}
                    />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Enter password"
                        onChange={(e) => {
                            setPassword(e.target.value)
                        }}
                    />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input
                        type="email"
                        className="form-control"
                        placeholder="Enter Email"
                        onChange={(e) => {
                            setEmail(e.target.value)
                        }}
                    />
                </div>

                <button
                    type="submit"
                    className="btn btn-primary btn-block"
                    onClick={submitReview}>Submit</button>

            </form>
      
        </div>
    )
}

export default Compony
