import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Profile from './component/Profile';
import Login from './component/loginSignup/LoginTest'
import Register from './component/loginSignup/RegisterTest'
import Company from './component/Compony'
import { useAuth0 } from '@auth0/auth0-react'
import LogoutButton from './component/loginSignup/LogoutButton'
import LoginButton from './component/loginSignup/LoginButton';

function App() {

    const { isAuthenticated, user } = useAuth0()
    return (
        < Router >
            <div className="App" >
                <nav className="navbar navbar-expand-lg navbar-light fixed-top">
                    <div className="container">
                        <Link className="navbar-brand" to={"/sign-in"}>Coupon System</Link>
                        <Link className="navbar-brand" to={"/Login"}>Login</Link>
                        <Link className="navbar-brand" to={"/Register"}>Register</Link>
                        <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    
                                        <LoginButton />
                                        
                                       
                                </li>
                                <li className="nav-item">
                                   
                                        <Link className="nav-link" to={"/Profile"}>
                                            Profile
                                        </Link>

                                        

                                </li>
                                <li className="nav-item">
                                
                                    <Link className="nav-link" to={"/Company"}>
                                        Company
                                    </Link>
                                    
                                    
                                </li>
                                <li className="nav-item">
                               
                                    <Link className="nav-link" to={"/Register"}>
                                        Company
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="auth-wrapper">
                   
                        <div className="auth-inner">
                            <Switch>
                                <Route path="/Profile" component={Profile} />
                                <Route path="/Company" component={Company}/>
                                <Route path="/Login" component={Login}/>
                                <Route path="/Register" component={Register}/>
                            </Switch>
                        </div>
                       
                </div>
            </div>
        </Router >
    );
}

export default App;