const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const mysql = require('mysql')

const db = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'couponsystemereact'
})

app.use(cors())

app.use(express.json())

app.use(bodyParser.urlencoded({ extended: true }))

app.post('/api/insert', (req, res) => {

    const compName = req.body.compName
    const password = req.body.password
    const email = req.body.email

    const sqlInsert = "INSERT INTO company (COMP_NAME, PASSWARD, EMAIL) VALUES (?,?,?)"
    db.query(sqlInsert, [compName, password, email], (err, result) => {
        console.log(err)
    })
})

app.get("/api/get", (req, res) => {

    const sqlSelect = "SELECT * FROM movie_review;"
    db.query(sqlSelect, (err, result) => {
        res.send(result)
    })
})

app.listen(3001, () => {
    console.log('3001')
})

//Register
app.post('/register', (req, res) => {

    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const email = req.body.email
    const password = req.body.password
    const confirmPassword = req.body.confirmPassword
    const isExists = false
    const gender = req.body.gender


    const sqlSelectEmail = "SELECT * FROM users WHERE Email = ? "
    db.query(sqlSelectEmail, [email], (err, result) => {

        if (result.length > 0) {

            res.send({ message: "this email already Exists" })
        } else {
            res.send("hello")
            const sqlInsertReg = "INSERT INTO users (firstName, lastName, email, password, radioLogin, timeCreated) VALUE (?,?,?,?,?,NOW())"
            db.query(sqlInsertReg, [firstName, lastName, email, password, radioLogin], (err, result2) => {
                res.send(result2)

            })
        }
    })


})

//Login
app.post("/login", (req, res) => {

    const userName = req.body.userName
    const password = req.body.password
    const radioLogin = req.body.radioLogin

    const sqlSelect = "SELECT * FROM users WHERE UserName = ? and Password = ? and radioLogin = ?"
    db.query(sqlSelect, [userName, password, radioLogin], (err, result) => {
        if (err) {
            res.send({ err: err })
        }
        if (result.length > 0) {
            res.send(result)
        } else {
            res.send({ message: "Worg username/password combination!" })
        }

    })

})